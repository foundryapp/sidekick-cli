import io from 'socket.io-client';

const port = '8011';
let socket: any;

async function connect() {
  await new Promise((resolve, reject) => {
    socket.once('connect', () => {
      return resolve();
    });
    socket.once('connect_error', (error: any) => {
      // TODO: Ask users whether we should start the app for them.
      // Or if the app isn't installed, provide the download link + ideally we download & install the app for them.
      return reject(new Error('Sidekick seems not to be running. Please start the Sidekick app.'));
    });
    socket.on('connect_timeout', (t: any) => {
      return reject(new Error(`Connection timeout ${t}`));
    });
    socket.on('error', (e: any) => {
      return reject(e);
    });
    // TODO: Add type to data
    socket.on('response', (data: any) => {
      const { status, response }: { status: number, response: any } = data;
      switch (status) {
        case 200:
          console.log('200, ok');
          break;
        case 400:
          const { message }: { message: string } = response;
          console.error(message);
          break;
      }
      socket.disconnect();
    });
    socket.connect();
  });
}

// Connect to the local terminal controller server
export async function sendMessage(receiver_username: string, message: string) {
  socket = io(`http://localhost:${port}/cli`, {
    transportOptions: {
      polling: {
        extraHeaders: {
          event: 'send_message',
          receiver_username,
          message,
        },
      },
    },
  });
  try {
    await connect();
  } catch (error) {
    console.log(error);
    socket.disconnect();
  }
}



