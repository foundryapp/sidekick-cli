#!/usr/local/bin/node

import { Command } from 'commander';
import * as conn from './connection';
const packageJSON = require('../package.json');

const program = new Command();
program.version(packageJSON.version, '-v, --version');

console.log(process.argv);
console.log('=============');

function parseArgvForSending(argv: string[]) {
  const copy = [...argv];
  const sysArg1 = copy.shift() as string;
  const sysArg2 = copy.shift() as string;

  const user = copy.shift() as string;
  const cmd = copy.join(' ');
  // console.log([user, cmd]);
  return [sysArg1, sysArg2, user, cmd];
}

/*
program
  .arguments('<user> <commands>')
  .action((user, command) => {
    msg.send(user, command);
  });
*/


program
  .command('msg <user> <command>')
  .action(async (user, command) => {
    conn.sendMessage(user, command);
  });

program.parse(process.argv);

/*
program
  .command('send <data>')
  .action((data) => {
    console.log('data', data);
  })
  .parse(process.argv);
*/


